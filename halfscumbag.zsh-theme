# ZSH Theme - Preview: http://cl.ly/350F0F0k1M2y3A2i3p1S

function my_git_prompt() {
  tester=$(git rev-parse --git-dir 2> /dev/null) || return
  
  INDEX=$(git status --porcelain 2> /dev/null)
  STATUS=""
  SUFFIX=""

  # is branch ahead?
  #if $(echo "$(git log origin/$(git_current_branch)..HEAD 2> /dev/null)" | grep '^commit' &> /dev/null); then
   # STATUS="$STATUS$ZSH_THEME_GIT_PROMPT_AHEAD"
  #fi

  # is anything staged?
  if $(echo "$INDEX" | command grep -E -e '^(D[ M]|[MARC][ MD]) ' &> /dev/null); then
    STATUS="$STATUS$ZSH_THEME_GIT_PROMPT_STAGED"
  fi

  # is anything unstaged?
  if $(echo "$INDEX" | command grep -E -e '^[ MARC][MD] ' &> /dev/null); then
    STATUS="$STATUS$ZSH_THEME_GIT_PROMPT_UNSTAGED"
  fi

  # is anything untracked?
  if $(echo "$INDEX" | grep '^?? ' &> /dev/null); then
    STATUS="$STATUS$ZSH_THEME_GIT_PROMPT_UNTRACKED"
    SUFFIX="$SUFFIX$ZSH_THEME_GIT_PROMPT_UNTRACKED_SUFFIX"
  fi

  # is anything unmerged?
  if $(echo "$INDEX" | command grep -E -e '^(A[AU]|D[DU]|U[ADU]) ' &> /dev/null); then
    STATUS="$STATUS$ZSH_THEME_GIT_PROMPT_UNMERGED"
  fi

  if [[ -n $STATUS ]]; then
    STATUS="$STATUS"
  fi

  echo "$ZSH_THEME_GIT_PROMPT_PREFIX$STATUS$(my_current_branch)$SUFFIX$ZSH_THEME_GIT_PROMPT_SUFFIX"
}

function my_current_branch() {
  echo $(git_current_branch || echo "(no branch)")
}

PROMPT=$'\n''%{$fg[blue]%} λ %{$reset_color%}${PWD/#$HOME/~} $(my_git_prompt)%{$reset_color%}'

ZSH_THEME_GIT_PROMPT_PREFIX="%{$fg[green]%}"
ZSH_THEME_GIT_PROMPT_AHEAD="%{fg[green]%}" #"$fg[blue]↑ %{$reset_color%}" #"%{$fg[cyan]%}"
ZSH_THEME_GIT_PROMPT_STAGED="%{$fg[yellow]%}"
ZSH_THEME_GIT_PROMPT_UNSTAGED="%{$fg[yellow]%}"
ZSH_THEME_GIT_PROMPT_UNTRACKED="%{$fg[yellow]%}"
ZSH_THEME_GIT_PROMPT_UNTRACKED_SUFFIX=""
ZSH_THEME_GIT_PROMPT_UNMERGED="%{$fg[yellow]%}"
ZSH_THEME_GIT_PROMPT_SUFFIX="%{$reset_color%} "

# '⚡'
# '→'
# '↩'
# '↑'
# '↓'
